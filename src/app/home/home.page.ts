import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MaskPipe } from 'ngx-mask';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  myForm: FormGroup;

  constructor(private zone: NgZone, public formBuilder: FormBuilder, public maskPipe: MaskPipe) {

    this.myForm = this.formBuilder.group({
      cep: [''],
      cpf: [''],
      telefone: [''],
    });
  }

  ngOnInit() {}

  updateWithMask(event) {
    const input = event.currentTarget.getAttribute('formControlName');

    switch (input) {
      case 'cep':
        this.myForm.controls.cep.setValue(this.maskPipe.transform(event.currentTarget.value, '00000-000'));
        break;

      case 'cpf':
        this.myForm.controls.cpf.setValue(this.maskPipe.transform(event.currentTarget.value, '000.000.000-00'));
        break;

      case 'telefone':
        if ( event.currentTarget.value.length > 13 ) {
          this.myForm.controls.telefone.setValue(this.maskPipe.transform(event.currentTarget.value, '(00)00000-0000'));
        } else {
          this.myForm.controls.telefone.setValue(this.maskPipe.transform(event.currentTarget.value, '(00)0000-0000'));
        }
        break;

      default:
        break;


    }
  }

}
